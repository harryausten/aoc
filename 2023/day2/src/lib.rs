use std::{
    fs::File,
    io::{BufRead, BufReader},
};

use nom::{bytes::complete::tag, IResult};

const NUM_RED: u32 = 12;
const NUM_GREEN: u32 = 13;
const NUM_BLUE: u32 = 14;

enum Cubes {
    Red(u32),
    Green(u32),
    Blue(u32),
}

impl Cubes {
    const fn possible(&self) -> bool {
        match *self {
            Self::Red(i) => i <= NUM_RED,
            Self::Green(i) => i <= NUM_GREEN,
            Self::Blue(i) => i <= NUM_BLUE,
        }
    }
}

fn parse_cubes(s: &str) -> IResult<&str, Cubes> {
    let (s, (num, colour)) = nom::sequence::separated_pair(
        nom::character::complete::u32,
        tag(" "),
        nom::character::complete::alpha1,
    )(s)?;
    let cubes = match colour {
        "red" => Cubes::Red(num),
        "green" => Cubes::Green(num),
        "blue" => Cubes::Blue(num),
        _ => unreachable!(),
    };

    Ok((s, cubes))
}

struct Game {
    index: u32,
    cubes: Vec<Vec<Cubes>>,
}

impl Game {
    fn possible(&self) -> bool {
        self.cubes.iter().all(|c| c.iter().all(Cubes::possible))
    }

    fn min_power(&self) -> u32 {
        let mut red = 0;
        let mut green = 0;
        let mut blue = 0;

        self.cubes.iter().for_each(|c| {
            c.iter().for_each(|c| match *c {
                Cubes::Red(i) => red = u32::max(i, red),
                Cubes::Green(i) => green = u32::max(i, green),
                Cubes::Blue(i) => blue = u32::max(i, blue),
            });
        });

        red * blue * green
    }
}

fn parse_game(s: &str) -> IResult<&str, Game> {
    let (s, _) = tag("Game ")(s)?;
    let (s, index) = nom::character::complete::u32(s)?;
    let (s, _) = tag(": ")(s)?;
    let (s, cubes) = nom::multi::separated_list1(
        tag("; "),
        nom::multi::separated_list1(tag(", "), parse_cubes),
    )(s)?;

    Ok((s, Game { index, cubes }))
}

#[derive(Clone, Copy)]
pub enum Part {
    One,
    Two,
}

pub fn run(path: &str, part: Part) -> anyhow::Result<u32> {
    let reader = BufReader::new(File::open(path)?);
    let total = reader
        .lines()
        .map(Result::<_, _>::unwrap)
        .fold(0, |mut acc, line| {
            let (_, game) = parse_game(&line).unwrap();
            match part {
                Part::One => {
                    if game.possible() {
                        acc += game.index;
                    }
                }
                Part::Two => acc += game.min_power(),
            }
            acc
        });

    Ok(total)
}

#[cfg(test)]
mod tests {
    use super::{run, Part};

    #[test]
    fn example() {
        assert_eq!(run("example", Part::One).unwrap(), 8);
    }

    #[test]
    fn part1() {
        assert_eq!(run("input", Part::One).unwrap(), 2476);
    }

    #[test]
    fn part2() {
        assert_eq!(run("input", Part::Two).unwrap(), 54911);
    }
}
