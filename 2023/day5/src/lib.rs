use std::{
    fs::File,
    io::{BufRead, BufReader},
};

use itertools::Itertools;
use nom::{
    bytes::complete::tag,
    character::complete::{self, space1},
    multi::separated_list1,
    IResult,
};

fn parse_seeds(s: &str) -> IResult<&str, Vec<u64>> {
    let (s, _) = tag("seeds: ")(s)?;
    separated_list1(space1, complete::u64)(s)
}

fn parse_range(s: &str) -> IResult<&str, (u64, u64, u64)> {
    let (s, dst_start) = complete::u64(s)?;
    let (s, _) = space1(s)?;
    let (s, src_start) = complete::u64(s)?;
    let (s, _) = space1(s)?;
    let (s, len) = complete::u64(s)?;

    Ok((s, (dst_start, src_start, len)))
}

fn translate(map: &[(u64, u64, u64)], index: u64) -> u64 {
    if let Some(range) = map
        .iter()
        .find(|(_, s, l)| *s <= index && index < (*s + *l))
    {
        range.0 + (index - range.1)
    } else {
        index
    }
}

fn min_location<I>(maps: &[Vec<(u64, u64, u64)>], seeds: I) -> u64
where
    I: Iterator<Item = u64>,
{
    seeds.fold(u64::MAX, |acc, seed| {
        acc.min(
            maps.iter()
                .fold(seed, |acc, map| translate(map.as_slice(), acc)),
        )
    })
}

fn translate_range(map: &[(u64, u64, u64)], index_range: (u64, u64)) -> Vec<(u64, u64)> {
    if let Some((d, s, l)) = map
        .iter()
        .find(|(_, s, l)| index_range.0 < s + l && *s < index_range.0 + index_range.1)
    {
        let mut ret = Vec::new();

        if index_range.0 < *s {
            ret.extend(translate_range(map, (index_range.0, s - index_range.0)));
        }

        let start = (*s).max(index_range.0) - s + d;
        let end = (s + l).min(index_range.0 + index_range.1) - s + d;
        ret.push((start, end - start));

        if index_range.0 + index_range.1 > s + l {
            ret.extend(translate_range(
                map,
                (s + l, index_range.0 + index_range.1 - s - l),
            ));
        }

        ret
    } else {
        vec![index_range]
    }
}

fn min_location_range(maps: &[Vec<(u64, u64, u64)>], seed_ranges: Vec<(u64, u64)>) -> u64 {
    maps.iter()
        .fold(seed_ranges, |acc, map| {
            acc.into_iter()
                .flat_map(|index_range| translate_range(map, index_range))
                .collect()
        })
        .into_iter()
        .map(|(s, _)| s)
        .min()
        .unwrap()
}

#[derive(Copy, Clone)]
pub enum Part {
    One,
    Two,
}

pub fn run(path: &str, part: Part) -> anyhow::Result<u64> {
    let reader = BufReader::new(File::open(path)?);
    let mut lines = reader.lines();
    let seeds = parse_seeds(&lines.next().unwrap()?).unwrap().1;
    let mut maps = vec![Vec::new()];

    let mut map_index = 0;
    for line in lines.skip(2) {
        let line = line?;

        if line.trim().is_empty() {
            continue;
        } else if line.chars().next().unwrap().is_ascii_alphabetic() {
            map_index += 1;
            maps.push(Vec::new());
        } else {
            maps[map_index].push(parse_range(&line).unwrap().1);
        }
    }

    let min = match part {
        Part::One => min_location(&maps, seeds.into_iter()),
        Part::Two => min_location_range(&maps, seeds.into_iter().tuples().collect()),
    };

    Ok(min)
}

#[cfg(test)]
mod tests {
    use super::{run, Part};

    #[test]
    fn example1() {
        assert_eq!(run("example", Part::One).unwrap(), 35);
    }

    #[test]
    fn part1() {
        assert_eq!(run("input", Part::One).unwrap(), 621354867);
    }

    #[test]
    fn example2() {
        assert_eq!(run("example", Part::Two).unwrap(), 46);
    }

    #[test]
    fn part2() {
        assert_eq!(run("input", Part::Two).unwrap(), 15880236);
    }
}
