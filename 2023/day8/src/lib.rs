use std::{
    collections::HashMap,
    fs::File,
    io::{BufRead, BufReader},
};

struct Node {
    left: [u8; 3],
    right: [u8; 3],
}

fn str_to_array(s: &str) -> [u8; 3] {
    s.as_bytes().try_into().unwrap()
}

fn parse_line(line: &str) -> ([u8; 3], Node) {
    let name = str_to_array(&line[0..3]);
    let left = str_to_array(&line[7..10]);
    let right = str_to_array(&line[12..15]);

    (name, Node { left, right })
}

const START: [u8; 3] = [b'A', b'A', b'A'];
const END: [u8; 3] = [b'Z', b'Z', b'Z'];

#[derive(Copy, Clone)]
pub enum Part {
    One,
    Two,
}

fn part1(map: HashMap<[u8; 3], Node>, instructions: String) -> usize {
    let mut current = map.get(&START).unwrap();
    let mut steps = 0;

    for c in instructions.chars().cycle() {
        let next = match c {
            'L' => current.left,
            'R' => current.right,
            _ => unreachable!(),
        };

        steps += 1;

        if next == END {
            break;
        }

        current = map.get(&next).unwrap();
    }

    steps
}

fn part2(map: HashMap<[u8; 3], Node>, instructions: String) -> usize {
    let mut current: Vec<_> = map
        .keys()
        .filter(|[_, _, c]| c == &b'A')
        .map(|name| map.get(name).unwrap())
        .collect();
    let mut steps = 0;

    for c in instructions.chars().cycle() {
        steps += 1;

        if current.iter_mut().fold(true, |mut end, node| {
            let name = match c {
                'L' => &node.left,
                'R' => &node.right,
                _ => unreachable!(),
            };
            end &= name[2] == b'Z';

            *node = map.get(name).unwrap();

            end
        }) {
            break;
        }
    }

    steps
}

pub fn run(path: &str, part: Part) -> anyhow::Result<usize> {
    let reader = BufReader::new(File::open(path)?);
    let mut lines = reader.lines();
    let instructions = lines.next().unwrap()?;

    lines.next();

    let mut map = HashMap::new();
    lines
        .map(|line| parse_line(&line.unwrap()))
        .for_each(|(name, node)| {
            map.insert(name, node);
        });

    Ok(match part {
        Part::One => part1(map, instructions),
        Part::Two => part2(map, instructions),
    })
}

#[cfg(test)]
mod tests {
    use super::{run, Part};

    #[test]
    fn example1() {
        assert_eq!(run("example1", Part::One).unwrap(), 2);
        assert_eq!(run("example2", Part::One).unwrap(), 6);
    }

    #[test]
    fn part1() {
        assert_eq!(run("input", Part::One).unwrap(), 17621);
    }

    #[test]
    fn example2() {
        assert_eq!(run("example3", Part::Two).unwrap(), 6);
    }

    #[test]
    // TODO: Currently too computationally expensive. Find a shortcut
    #[ignore]
    fn part2() {
        assert_eq!(run("input", Part::Two).unwrap(), 17621);
    }
}
