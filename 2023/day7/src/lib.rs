use std::{
    cmp::Ordering,
    collections::HashMap,
    fs::File,
    io::{BufRead, BufReader},
};

use nom::{
    character::complete::{self, alphanumeric1},
    IResult,
};

#[derive(PartialEq, Eq, PartialOrd, Ord, Copy, Clone, Hash)]
enum Card {
    Joker,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    Ten,
    Queen,
    King,
    Ace,
}

impl TryFrom<u8> for Card {
    type Error = ();

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        Ok(match value {
            b'J' => Self::Joker,
            b'2' => Self::Two,
            b'3' => Self::Three,
            b'4' => Self::Four,
            b'5' => Self::Five,
            b'6' => Self::Six,
            b'7' => Self::Seven,
            b'8' => Self::Eight,
            b'9' => Self::Nine,
            b'T' => Self::Ten,
            b'Q' => Self::Queen,
            b'K' => Self::King,
            b'A' => Self::Ace,
            _ => return Err(()),
        })
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Copy, Clone)]
enum HandType {
    HighCard,
    OnePair,
    TwoPair,
    ThreeOfAKind,
    FullHouse,
    FourOfAKind,
    FiveOfAKind,
}

impl From<&[Card; 5]> for HandType {
    fn from(hand: &[Card; 5]) -> Self {
        let mut occurences = HashMap::new();

        hand.iter().for_each(|&card| {
            occurences.entry(card).and_modify(|n| *n += 1).or_insert(1);
        });

        let mut occurences: Vec<(Card, usize)> = occurences.into_iter().collect();
        occurences.sort_unstable_by(|(c1, v1), (c2, v2)| v2.cmp(v1).then(c1.cmp(c2)));

        match (occurences.first(), occurences.get(1), occurences.get(2)) {
            (Some((_, 5)), _, _) => Self::FiveOfAKind,
            (Some((_, 4)), Some((Card::Joker, 1)), _) => Self::FiveOfAKind,
            (Some((_, 3)), Some((Card::Joker, 2)), _) => Self::FiveOfAKind,
            (Some((Card::Joker, 3)), Some((_, 2)), _) => Self::FiveOfAKind,
            (Some((Card::Joker, 4)), _, _) => Self::FiveOfAKind,
            (Some((_, 4)), _, _) => Self::FourOfAKind,
            (Some((_, 3)), Some((Card::Joker, 1)), _) => Self::FourOfAKind,
            (Some((Card::Joker, 2)), Some((_, 2)), _) => Self::FourOfAKind,
            (Some((Card::Joker, 3)), _, _) => Self::FourOfAKind,
            (Some((_, 3)), Some((_, 2)), _) => Self::FullHouse,
            (Some((_, 2)), Some((_, 2)), Some((Card::Joker, 1))) => Self::FullHouse,
            (Some((_, 3)), _, _) => Self::ThreeOfAKind,
            (Some((_, 2)), Some((Card::Joker, 1)), _) => Self::ThreeOfAKind,
            (Some((Card::Joker, 2)), _, _) => Self::ThreeOfAKind,
            (Some((_, 2)), Some((_, 2)), _) => Self::TwoPair,
            (Some((_, 2)), _, _) => Self::OnePair,
            (Some((Card::Joker, 1)), _, _) => Self::OnePair,
            _ => Self::HighCard,
        }
    }
}

#[derive(Eq)]
struct Hand {
    cards: [Card; 5],
    bid: u64,
    ty: HandType,
}

impl Hand {
    fn new(cards: &str, bid: u64) -> Self {
        let mut arr = [Card::Ace; 5];
        cards
            .bytes()
            .map(|b| Card::try_from(b).unwrap())
            .zip(arr.iter_mut())
            .for_each(|(card, entry)| *entry = card);
        let ty = (&arr).into();

        Self {
            cards: arr,
            bid,
            ty,
        }
    }
}

impl PartialEq for Hand {
    fn eq(&self, other: &Self) -> bool {
        self.cards == other.cards
    }
}

impl PartialOrd for Hand {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Hand {
    fn cmp(&self, other: &Self) -> Ordering {
        (self.ty, self.cards).cmp(&(other.ty, other.cards))
    }
}

fn parse_hand(s: &str) -> IResult<&str, Hand> {
    let (s, cards) = alphanumeric1(s)?;
    let (s, bid) = complete::u64(&s[1..])?;

    Ok((s, Hand::new(cards, bid)))
}

pub fn run(path: &str) -> anyhow::Result<usize> {
    let reader = BufReader::new(File::open(path)?);
    let mut hands: Vec<_> = reader
        .lines()
        .map(|line| parse_hand(&line.unwrap()).unwrap().1)
        .collect();
    hands.sort_unstable();

    Ok(hands
        .into_iter()
        .enumerate()
        .map(|(i, h)| (i + 1) * (h.bid as usize))
        .sum())
}

#[cfg(test)]
mod tests {
    use super::run;

    #[test]
    #[ignore]
    fn example1() {
        assert_eq!(run("example").unwrap(), 6440);
    }

    #[test]
    #[ignore]
    fn part1() {
        assert_eq!(run("input").unwrap(), 251121738);
    }

    #[test]
    fn example2() {
        assert_eq!(run("example").unwrap(), 5905);
    }

    #[test]
    fn part2() {
        assert_eq!(run("input").unwrap(), 251421071);
    }
}
