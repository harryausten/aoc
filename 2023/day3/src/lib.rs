use std::{
    fs::File,
    io::{BufRead, BufReader},
};

struct Symbol {
    x: usize,
    y: usize,
}

struct Number {
    num: u32,
    x1: usize,
    x2: usize,
    y: usize,
}

impl Number {
    fn adjacent(&self, s: &Symbol) -> bool {
        (s.x.abs_diff(self.x1) <= 1 || s.x.abs_diff(self.x2) <= 1) && s.y.abs_diff(self.y) <= 1
    }
}

#[derive(Clone, Copy)]
pub enum Part {
    One,
    Two,
}

pub fn run(path: &str, part: Part) -> anyhow::Result<u32> {
    let reader = BufReader::new(File::open(path)?);
    let mut numbers: Vec<Number> = Vec::new();
    let mut symbols = Vec::new();
    let mut y = 0;

    reader.lines().map(Result::<_, _>::unwrap).for_each(|line| {
        let mut in_number = false;
        line.chars().enumerate().for_each(|(x, c)| match c {
            d if d.is_ascii_digit() && in_number => numbers.last_mut().unwrap().x2 = x,
            d if d.is_ascii_digit() => {
                numbers.push(Number {
                    num: 0,
                    x1: x,
                    x2: x,
                    y,
                });
                in_number = true;
            }
            '.' => in_number = false,
            _ => {
                symbols.push(Symbol { x, y });
                in_number = false;
            }
        });
        numbers.iter_mut().filter(|n| n.y == y).for_each(|n| {
            n.num = line.as_str()[n.x1..=n.x2].parse::<u32>().unwrap();
        });
        y += 1;
    });

    let total = match part {
        Part::One => numbers
            .iter()
            .filter(|n| symbols.iter().any(|s| n.adjacent(s)))
            .map(|n| n.num)
            .sum(),
        Part::Two => symbols
            .iter()
            .filter_map(|s| {
                let adjacent_nums = numbers.iter().filter(|n| n.adjacent(s));
                if adjacent_nums.clone().count() == 2 {
                    Some(adjacent_nums.fold(1, |acc, n| acc * n.num))
                } else {
                    None
                }
            })
            .sum(),
    };

    Ok(total)
}

#[cfg(test)]
mod tests {
    use super::{run, Part};

    #[test]
    fn example1() {
        assert_eq!(run("example", Part::One).unwrap(), 4361);
    }

    #[test]
    fn part1() {
        assert_eq!(run("input", Part::One).unwrap(), 532428);
    }

    #[test]
    fn example2() {
        assert_eq!(run("example", Part::Two).unwrap(), 467835);
    }

    #[test]
    fn part2() {
        assert_eq!(run("input", Part::Two).unwrap(), 84051670);
    }
}
