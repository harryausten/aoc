use std::{
    collections::HashMap,
    fs::File,
    io::{BufRead, BufReader},
};

use nom::{
    bytes::complete::tag,
    character::complete::{self, space1},
    multi::many1,
    IResult,
};

struct Card {
    index: u32,
    count: usize,
}

impl Card {
    fn new(index: u32, winning_numbers: Vec<u8>, own_numbers: Vec<u8>) -> Self {
        let count = own_numbers
            .iter()
            .filter(|n| winning_numbers.contains(n))
            .count();

        Self { index, count }
    }
}

fn parse_number(s: &str) -> IResult<&str, u8> {
    let (s, _) = space1(s)?;
    complete::u8(s)
}

fn parse_card(s: &str) -> IResult<&str, Card> {
    let (s, _) = tag("Card")(s)?;
    let (s, _) = space1(s)?;
    let (s, index) = complete::u32(s)?;
    let (s, _) = tag(":")(s)?;
    let (s, winning_numbers) = many1(parse_number)(s)?;
    let (s, _) = tag(" |")(s)?;
    let (s, own_numbers) = many1(parse_number)(s)?;

    Ok((s, Card::new(index, winning_numbers, own_numbers)))
}

#[derive(Clone, Copy)]
pub enum Part {
    One,
    Two,
}

pub fn run(path: &str, part: Part) -> anyhow::Result<usize> {
    let reader = BufReader::new(File::open(path)?);

    let cards = reader
        .lines()
        .map(Result::<_, _>::unwrap)
        .map(|line| parse_card(&line).unwrap().1);

    let total = match part {
        Part::One => cards
            .map(|c| {
                if c.count == 0 {
                    0
                } else {
                    1usize << (c.count - 1)
                }
            })
            .sum(),
        Part::Two => {
            let mut won_cards = HashMap::new();
            cards.fold(0, |acc, c| {
                let index = c.index as usize;
                let num = 1 + won_cards.get(&index).unwrap_or(&0);
                let next = index + 1;
                (next..next + c.count).for_each(|i| {
                    won_cards.entry(i).and_modify(|n| *n += num).or_insert(num);
                });
                acc + num
            })
        }
    };

    Ok(total)
}

#[cfg(test)]
mod tests {
    use super::{run, Part};

    #[test]
    fn example1() {
        assert_eq!(run("example", Part::One).unwrap(), 13);
    }

    #[test]
    fn part1() {
        assert_eq!(run("input", Part::One).unwrap(), 24175);
    }

    #[test]
    fn example2() {
        assert_eq!(run("example", Part::Two).unwrap(), 30);
    }

    #[test]
    fn part2() {
        assert_eq!(run("input", Part::Two).unwrap(), 18846301);
    }
}
