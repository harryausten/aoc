pub fn run(path: &str) -> anyhow::Result<u32> {
    let text = std::fs::read_to_string(path)?;
    let total = text.lines().fold(0, |mut acc, line| {
        let front = line
            .chars()
            .find(char::is_ascii_digit)
            .expect("line must have at least one number")
            .to_digit(10)
            .expect("already checked digit");
        let back = line
            .chars()
            .rev()
            .find(char::is_ascii_digit)
            .expect("line must have at least one number")
            .to_digit(10)
            .expect("already checked digit");
        acc += front * 10 + back;
        acc
    });

    Ok(total)
}

#[cfg(test)]
mod tests {
    use super::run;

    #[test]
    fn example() {
        assert_eq!(run("example").unwrap(), 142);
    }

    #[test]
    fn part1() {
        assert_eq!(run("input").unwrap(), 54916);
    }
}
