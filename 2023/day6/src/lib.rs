use std::{
    fs::File,
    io::{BufRead, BufReader},
};

use nom::{
    bytes::complete::tag,
    character::complete::{self, space1},
    multi::separated_list1,
    IResult,
};

fn parse_line<'a>(s: &'a str, tagname: &'static str) -> IResult<&'a str, Vec<u64>> {
    let (s, _) = tag(tagname)(s)?;
    let (s, _) = space1(s)?;
    separated_list1(space1, complete::u64)(s)
}

fn num_better_options(total_time: u64, record: u64) -> usize {
    // Solve quadratic equation for t in:
    // record = t * (total_time - t)
    let total_time = total_time as f64;
    let record = record as f64;
    let disc = (total_time * total_time - 4f64 * record).sqrt();
    let min = (total_time - disc) / 2f64;
    let max = (total_time + disc) / 2f64;
    // Get next f64 along to ensure we get the adjacent integer
    let min = (f64::from_bits(min.to_bits() + 1)).ceil() as usize;
    let max = (f64::from_bits(max.to_bits() - 1)).floor() as usize;

    max - min + 1
}

#[derive(Copy, Clone)]
pub enum Part {
    One,
    Two,
}

pub fn run(path: &str, part: Part) -> anyhow::Result<usize> {
    let reader = BufReader::new(File::open(path)?);
    let mut lines = reader.lines();

    let result = match part {
        Part::One => {
            let time = parse_line(&lines.next().unwrap()?, "Time:").unwrap().1;
            let distance = parse_line(&lines.next().unwrap()?, "Distance:").unwrap().1;
            time.into_iter()
                .zip(distance)
                .map(|(t, d)| num_better_options(t, d))
                .product()
        }
        Part::Two => {
            let time = lines
                .next()
                .unwrap()?
                .chars()
                .filter(char::is_ascii_digit)
                .collect::<String>()
                .parse()?;
            let distance = lines
                .next()
                .unwrap()?
                .chars()
                .filter(char::is_ascii_digit)
                .collect::<String>()
                .parse()?;
            num_better_options(time, distance)
        }
    };

    Ok(result)
}

#[cfg(test)]
mod tests {
    use super::{run, Part};

    #[test]
    fn example1() {
        assert_eq!(run("example", Part::One).unwrap(), 288);
    }

    #[test]
    fn part1() {
        assert_eq!(run("input", Part::One).unwrap(), 160816);
    }

    #[test]
    fn example2() {
        assert_eq!(run("example", Part::Two).unwrap(), 71503);
    }

    #[test]
    fn part2() {
        assert_eq!(run("input", Part::Two).unwrap(), 46561107);
    }
}
