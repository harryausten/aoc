pub fn is_safe<T>(report: T) -> bool
where
    T: Iterator<Item = usize> + Clone,
{
    let in_order =
        report.clone().is_sorted_by(|a, b| a < b) || report.clone().is_sorted_by(|a, b| a > b);
    let max_diff = report
        .clone()
        .zip(report.skip(1))
        .map(|(x, y)| x.abs_diff(y))
        .max()
        .unwrap();

    in_order && (1..=3).contains(&max_diff)
}

pub fn part1(input: &str) -> usize {
    input
        .lines()
        .filter(|line| {
            let report = line.split_whitespace().map(|n| n.parse::<usize>().unwrap());
            is_safe(report)
        })
        .count()
}

pub fn part2(input: &str) -> usize {
    input
        .lines()
        .filter(|line| {
            let report = line.split_whitespace().map(|n| n.parse::<usize>().unwrap());
            if is_safe(report.clone()) {
                return true;
            }

            let report = report.collect::<Vec<_>>();
            (0..report.len()).any(|i| {
                let mut reduced_report = report.clone();
                reduced_report.remove(i);
                is_safe(reduced_report.into_iter())
            })
        })
        .count()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn part1_example() {
        assert_eq!(part1(include_str!("example")), 2);
    }

    #[test]
    fn part1_input() {
        assert_eq!(part1(include_str!("input")), 524);
    }

    #[test]
    fn part2_example() {
        assert_eq!(part2(include_str!("example")), 4);
    }

    #[test]
    fn part2_input() {
        assert_eq!(part2(include_str!("input")), 569);
    }
}
