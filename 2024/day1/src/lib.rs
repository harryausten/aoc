pub fn parse_lists(input: &str) -> (Vec<usize>, Vec<usize>) {
    let list1 = input
        .lines()
        .map(|l| l.split_once(' ').unwrap().0.parse::<usize>().unwrap())
        .collect::<Vec<_>>();
    let list2 = input
        .lines()
        .map(|l| l.rsplit_once(' ').unwrap().1.parse::<usize>().unwrap())
        .collect::<Vec<_>>();
    (list1, list2)
}

pub fn part1(input: &str) -> usize {
    let (mut list1, mut list2) = parse_lists(input);
    list1.sort_unstable();
    list2.sort_unstable();

    list1
        .into_iter()
        .zip(list2)
        .fold(0, |acc, e| acc + e.0.abs_diff(e.1))
}

pub fn part2(input: &str) -> usize {
    let (list1, list2) = parse_lists(input);

    list1.into_iter().fold(0, |acc, x| {
        acc + x * list2.iter().filter(|y| **y == x).count()
    })
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn part1_example() {
        assert_eq!(part1(include_str!("example")), 11);
    }

    #[test]
    fn part1_input() {
        assert_eq!(part1(include_str!("input")), 1879048);
    }

    #[test]
    fn part2_example() {
        assert_eq!(part2(include_str!("example")), 31);
    }

    #[test]
    fn part2_input() {
        assert_eq!(part2(include_str!("input")), 21024792);
    }
}
